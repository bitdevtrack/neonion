from settings.default import *

# neonion specific
ANNOTATION_STORE_URL = "http://annotator.neonion.imp.fu-berlin.de"
ELASTICSEARCH_URL = "http://localhost:9200"
ENDPOINT = 'http://tom.imp.fu-berlin.de:8880/openrdf-sesame/repositories/neonion'
ENDPOINT_UPDATE = 'http://tom.imp.fu-berlin.de:8880/openrdf-sesame/repositories/neonion/statements'
DEFAULT_USER_ACTIVE_STATE = False